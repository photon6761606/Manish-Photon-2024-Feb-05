//
//  NYCSchool.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/5/24.
//

import Foundation
import SafariServices

struct NYCSchool: Identifiable, Decodable, Hashable {
    var id: UUID = UUID()
    var dbn: String = ""
    var school_name: String = ""
    var boro: String = ""
    var overview_paragraph: String = ""
    var school_10th_seats: String = ""
    var academicopportunities1: String = ""
    var academicopportunities2: String = ""
    var ell_programs: String = ""
    var neighborhood: String = ""
    var building_code: String = ""
    var location: String = ""
    var phone_number: String = ""
    var fax_number: String = ""
    var school_email: String = ""
    var website: String = ""
    var subway: String = ""
    var bus: String = ""
    var finalgrades: String = ""
    var total_students: String = ""
    var extracurricular_activities: String = ""
    var school_sports: String = ""
    var attendance_rate: String = ""
    var pct_stu_enough_variety: String = ""
    var pct_stu_safe: String = ""
    var school_accessibility_description: String = ""
    var directions1: String = ""
    var requirement1_1: String = ""
    var requirement2_1: String = ""
    var requirement3_1: String = ""
    var requirement4_1: String = ""
    var requirement5_1: String = ""
    var offer_rate1: String = ""
    var program1: String = ""
    var code1: String = ""
    var interest1: String = ""
    var method1: String = ""
    var seats9ge1: String = ""
    var grade9gefilledflag1: String = ""
    var grade9geapplicants1: String = ""
    var seats9swd1: String = ""
    var grade9swdfilledflag1: String = ""
    var grade9swdapplicants1: String = ""
    var seats101: String = ""
    var admissionspriority11: String = ""
    var admissionspriority21: String = ""
    var admissionspriority31: String = ""
    var grade9geapplicantsperseat1: String = ""
    var grade9swdapplicantsperseat1: String = ""
    var primary_address_line_1: String = ""
    var city: String = ""
    var zip: String = ""
    var state_code: String = ""
    var latitude: String = ""
    var longitude: String = ""
    var community_board: String = ""
    var council_district: String = ""
    var census_tract: String = ""
    var bin: String = ""
    var bbl: String = ""
    var nta: String = ""
    var borough: String = ""
    
    enum CodingKeys: CodingKey {
        case dbn
        case school_name
        case boro
        case overview_paragraph
        case school_10th_seats
        case academicopportunities1
        case academicopportunities2
        case ell_programs
        case neighborhood
        case building_code
        case location
        case phone_number
        case fax_number
        case school_email
        case website
        case subway
        case bus
        case finalgrades
        case total_students
        case extracurricular_activities
        case school_sports
        case attendance_rate
        case pct_stu_enough_variety
        case pct_stu_safe
        case school_accessibility_description
        case directions1
        case requirement1_1
        case requirement2_1
        case requirement3_1
        case requirement4_1
        case requirement5_1
        case offer_rate1
        case program1
        case code1
        case interest1
        case method1
        case seats9ge1
        case grade9gefilledflag1
        case grade9geapplicants1
        case seats9swd1
        case grade9swdfilledflag1
        case grade9swdapplicants1
        case seats101
        case admissionspriority11
        case admissionspriority21
        case admissionspriority31
        case grade9geapplicantsperseat1
        case grade9swdapplicantsperseat1
        case primary_address_line_1
        case city
        case zip
        case state_code
        case latitude
        case longitude
        case community_board
        case council_district
        case census_tract
        case bin
        case bbl
        case nta
        case borough
    }
 
    init() {}

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.dbn = try container.decode(String.self, forKey: .dbn)
        self.school_name = try container.decodeIfPresent(String.self, forKey: .school_name) ?? ""
        self.boro = try container.decodeIfPresent(String.self, forKey: .boro) ?? ""
        self.overview_paragraph = try container.decodeIfPresent(String.self, forKey: .overview_paragraph) ?? ""
        self.school_10th_seats = try container.decodeIfPresent(String.self, forKey: .school_10th_seats) ?? ""
        self.academicopportunities1 = try container.decodeIfPresent(String.self, forKey: .academicopportunities1) ?? ""
        self.academicopportunities2 = try container.decodeIfPresent(String.self, forKey: .academicopportunities2) ?? ""
        self.ell_programs = try container.decodeIfPresent(String.self, forKey: .ell_programs) ?? ""
        self.neighborhood = try container.decodeIfPresent(String.self, forKey: .neighborhood) ?? ""
        self.building_code = try container.decodeIfPresent(String.self, forKey: .building_code) ?? ""
        self.location = try container.decodeIfPresent(String.self, forKey: .location) ?? ""
        self.phone_number = try container.decodeIfPresent(String.self, forKey: .phone_number) ?? ""
        self.fax_number = try container.decodeIfPresent(String.self, forKey: .fax_number) ?? ""
        self.school_email = try container.decodeIfPresent(String.self, forKey: .school_email) ?? ""
        self.website = try container.decodeIfPresent(String.self, forKey: .website) ?? ""
        self.subway = try container.decodeIfPresent(String.self, forKey: .subway) ?? ""
        self.bus = try container.decodeIfPresent(String.self, forKey: .bus) ?? ""
        self.finalgrades = try container.decodeIfPresent(String.self, forKey: .finalgrades) ?? ""
        self.total_students = try container.decodeIfPresent(String.self, forKey: .total_students) ?? ""
        self.extracurricular_activities = try container.decodeIfPresent(String.self, forKey: .extracurricular_activities) ?? ""
        self.school_sports = try container.decodeIfPresent(String.self, forKey: .school_sports) ?? ""
        self.attendance_rate = try container.decodeIfPresent(String.self, forKey: .attendance_rate) ?? ""
        self.pct_stu_enough_variety = try container.decodeIfPresent(String.self, forKey: .pct_stu_enough_variety) ?? ""
        self.pct_stu_safe = try container.decodeIfPresent(String.self, forKey: .pct_stu_safe) ?? ""
        self.school_accessibility_description = try container.decodeIfPresent(String.self, forKey: .school_accessibility_description) ?? ""
        self.directions1 = try container.decodeIfPresent(String.self, forKey: .directions1) ?? ""
        self.requirement1_1 = try container.decodeIfPresent(String.self, forKey: .requirement1_1) ?? ""
        self.requirement2_1 = try container.decodeIfPresent(String.self, forKey: .requirement2_1) ?? ""
        self.requirement3_1 = try container.decodeIfPresent(String.self, forKey: .requirement3_1) ?? ""
        self.requirement4_1 = try container.decodeIfPresent(String.self, forKey: .requirement4_1) ?? ""
        self.requirement5_1 = try container.decodeIfPresent(String.self, forKey: .requirement5_1) ?? ""
        self.offer_rate1 = try container.decodeIfPresent(String.self, forKey: .offer_rate1) ?? ""
        self.program1 = try container.decodeIfPresent(String.self, forKey: .program1) ?? ""
        self.code1 = try container.decodeIfPresent(String.self, forKey: .code1) ?? ""
        self.interest1 = try container.decodeIfPresent(String.self, forKey: .interest1) ?? ""
        self.method1 = try container.decodeIfPresent(String.self, forKey: .method1) ?? ""
        self.seats9ge1 = try container.decodeIfPresent(String.self, forKey: .seats9ge1) ?? ""
        self.grade9gefilledflag1 = try container.decodeIfPresent(String.self, forKey: .grade9gefilledflag1) ?? ""
        self.grade9geapplicants1 = try container.decodeIfPresent(String.self, forKey: .grade9geapplicants1) ?? ""
        self.seats9swd1 = try container.decodeIfPresent(String.self, forKey: .seats9swd1) ?? ""
        self.grade9swdfilledflag1 = try container.decodeIfPresent(String.self, forKey: .grade9swdfilledflag1) ?? ""
        self.grade9swdapplicants1 = try container.decodeIfPresent(String.self, forKey: .grade9swdapplicants1) ?? ""
        self.seats101 = try container.decodeIfPresent(String.self, forKey: .seats101) ?? ""
        self.admissionspriority11 = try container.decodeIfPresent(String.self, forKey: .admissionspriority11) ?? ""
        self.admissionspriority21 = try container.decodeIfPresent(String.self, forKey: .admissionspriority21) ?? ""
        self.admissionspriority31 = try container.decodeIfPresent(String.self, forKey: .admissionspriority31) ?? ""
        self.grade9geapplicantsperseat1 = try container.decodeIfPresent(String.self, forKey: .grade9geapplicantsperseat1) ?? ""
        self.grade9swdapplicantsperseat1 = try container.decodeIfPresent(String.self, forKey: .grade9swdapplicantsperseat1) ?? ""
        self.primary_address_line_1 = try container.decodeIfPresent(String.self, forKey: .primary_address_line_1)?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        self.city = try container.decodeIfPresent(String.self, forKey: .city) ?? ""
        self.zip = try container.decodeIfPresent(String.self, forKey: .zip) ?? ""
        self.state_code = try container.decodeIfPresent(String.self, forKey: .state_code) ?? ""
        self.latitude = try container.decodeIfPresent(String.self, forKey: .latitude) ?? ""
        self.longitude = try container.decodeIfPresent(String.self, forKey: .longitude) ?? ""
        self.community_board = try container.decodeIfPresent(String.self, forKey: .community_board) ?? ""
        self.council_district = try container.decodeIfPresent(String.self, forKey: .council_district) ?? ""
        self.census_tract = try container.decodeIfPresent(String.self, forKey: .census_tract) ?? ""
        self.bin = try container.decodeIfPresent(String.self, forKey: .bin) ?? ""
        self.bbl = try container.decodeIfPresent(String.self, forKey: .bbl) ?? ""
        self.nta = try container.decodeIfPresent(String.self, forKey: .nta)?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        self.borough = try container.decodeIfPresent(String.self, forKey: .borough)?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
    
    func tappedOnWebsite(url: String) {
        if var theURL = URL(string: url) {
            if theURL.scheme == nil {
                theURL = URL(string: "https://" + theURL.absoluteString)!
            }
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            UIApplication.shared.open(theURL)
        }
    }
}

/*
 {
 "dbn": "02M260",
 "school_name": "Clinton School Writers & Artists, M.S. 260",
 "boro": "M",
 "overview_paragraph": "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
 "school_10th_seats": "1",
 "academicopportunities1": "Free college courses at neighboring universities",
 "academicopportunities2": "International Travel, Special Arts Programs, Music, Internships, College Mentoring English Language Learner Programs: English as a New Language",
 "ell_programs": "English as a New Language",
 "neighborhood": "Chelsea-Union Sq",
 "building_code": "M868",
 "location": "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
 "phone_number": "212-524-4360",
 "fax_number": "212-524-4365",
 "school_email": "admissions@theclintonschool.net",
 "website": "www.theclintonschool.net",
 "subway": "1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St",
 "bus": "BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9",
 "grades2018": "6-11",
 "finalgrades": "6-12",
 "total_students": "376",
 "extracurricular_activities": "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee",
 "school_sports": "Cross Country, Track and Field, Soccer, Flag Football, Basketball",
 "attendance_rate": "0.970000029",
 "pct_stu_enough_variety": "0.899999976",
 "pct_stu_safe": "0.970000029",
 "school_accessibility_description": "1",
 "directions1": "See theclintonschool.net for more information.",
 "requirement1_1": "Course Grades: English (87-100), Math (83-100), Social Studies (90-100), Science (88-100)",
 "requirement2_1": "Standardized Test Scores: English Language Arts (2.8-4.5), Math (2.8-4.5)",
 "requirement3_1": "Attendance and Punctuality",
 "requirement4_1": "Writing Exercise",
 "requirement5_1": "Group Interview (On-Site)",
 "offer_rate1": "Â—57% of offers went to this group",
 "program1": "M.S. 260 Clinton School Writers & Artists",
 "code1": "M64A",
 "interest1": "Humanities & Interdisciplinary",
 "method1": "Screened",
 "seats9ge1": "80",
 "grade9gefilledflag1": "Y",
 "grade9geapplicants1": "1515",
 "seats9swd1": "16",
 "grade9swdfilledflag1": "Y",
 "grade9swdapplicants1": "138",
 "seats101": "YesÂ–9",
 "admissionspriority11": "Priority to continuing 8th graders",
 "admissionspriority21": "Then to Manhattan students or residents",
 "admissionspriority31": "Then to New York City residents",
 "grade9geapplicantsperseat1": "19",
 "grade9swdapplicantsperseat1": "9",
 "primary_address_line_1": "10 East 15th Street",
 "city": "Manhattan",
 "zip": "10003",
 "state_code": "NY",
 "latitude": "40.73653",
 "longitude": "-73.9927",
 "community_board": "5",
 "council_district": "2",
 "census_tract": "52",
 "bin": "1089902",
 "bbl": "1008420034",
 "nta": "Hudson Yards-Chelsea-Flatiron-Union Square                                 ",
 "borough": "MANHATTAN"
 }
*/
