//
//  Constants.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/5/24.
//

import Foundation

struct Constants {
    static var nycURL: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
}
