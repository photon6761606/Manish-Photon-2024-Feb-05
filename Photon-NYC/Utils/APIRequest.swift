//
//  APIRequest.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/5/24.
//

import Foundation

/// API Class to call any service
class APIRequest {
    
    // MARK: Methods
    func callService<T: Decodable>(url: String,
                                   forType: T.Type,
                                   returnObject: @escaping ([T]?, NSError?) -> Void) {
        if let unwrappedURL = URL(string: url) {
            let request = URLRequest(url: unwrappedURL)
            URLSession.shared.dataTask(with: request) { data, response, error in
                if error != nil {
                    returnObject(nil, error as? NSError)
                }
                
                if let data = data {
                    do {
                        let jsonDecoder = JSONDecoder()
                        returnObject(try jsonDecoder.decode([T].self, from: data), nil)
                    } catch {
                        print(error)
                    }
                }
            }.resume()
        }
    }
}
