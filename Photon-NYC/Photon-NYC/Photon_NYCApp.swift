//
//  Photon_NYCApp.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/6/24.
//

import SwiftUI

@main
struct Photon_NYCApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
