//
//  ContentView.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/5/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            NYCSchoolView()
        }.navigationTitle("NYC Schools")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
