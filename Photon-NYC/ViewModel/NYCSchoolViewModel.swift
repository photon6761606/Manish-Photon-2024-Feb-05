//
//  NYCSchoolViewModel.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/5/24.
//

import Foundation

class NYCSchoolViewModel: ObservableObject {
    
    @Published var schools: [NYCSchool] = []
    
    init() {
        fetchSchools()
    }
    
    func fetchSchools() {
        APIRequest().callService(url: Constants.nycURL,
                                 forType: NYCSchool.self) { schools, error in
            DispatchQueue.main.async {
                if let schools = schools {
                    self.schools = schools
                }
            }
        }
    }
}
