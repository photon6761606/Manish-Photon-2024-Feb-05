//
//  SchoolDetailsView.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/5/24.
//

import SwiftUI

struct SchoolDetailsView: View {
    var model: NYCSchool
    var body: some View {
        List {
            Text("About - \(model.school_name):")
                .fontWeight(.semibold)
            Text(model.overview_paragraph)
            VStack {
                if model.requirement1_1 != "" {
                    Text("Below are the requirements for this school:")
                        .fontWeight(.light)
                    Text("")
                    Text(model.requirement1_1)
                        .fontWeight(.bold)
                    Text(model.requirement2_1)
                    Text(model.requirement3_1)
                    Text(model.requirement4_1)
                    Text(model.requirement5_1)
                }
            }
            VStack(alignment: .leading) {
                if model.community_board != "" {
                    HStack {
                        Text("Community Board")
                            .fontWeight(.bold)
                        Text(model.community_board)
                    }
                    HStack {
                        Text("Council District")
                            .fontWeight(.bold)
                        Text(model.council_district)
                    }
                    
                    HStack {
                        Text("Census Trac")
                            .fontWeight(.bold)
                        Text(model.census_tract)
                    }
                    HStack {
                        Text("Final Grades")
                            .fontWeight(.bold)
                        Text(model.finalgrades)
                    }
                }
            }
        }.padding()
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView(model: NYCSchool())
    }
}
