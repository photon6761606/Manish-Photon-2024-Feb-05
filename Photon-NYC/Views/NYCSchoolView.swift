//
//  NYCSchoolView.swift
//  Photon-NYC
//
//  Created by Manish Gupta on 2/5/24.
//

import SwiftUI
import SafariServices

struct NYCSchoolView: View {
    @ObservedObject var viewModel = NYCSchoolViewModel()
    var body: some View {
        List(viewModel.schools, id: \.self) { element in
            NavigationLink {
                SchoolDetailsView(model: element)
            } label: {
                VStack(alignment: .leading) {
                    Text(element.school_name)
                        .font(.headline)
                    HStack {
                        Text("(\(element.primary_address_line_1),  \(element.borough))")
                    }.font(.caption2)
                    Spacer()
                    Text("Opportunities For:")
                        .fontWeight(.semibold)
                    Text(element.academicopportunities1)
                    Text(element.website)
                        .underline()
                        .foregroundColor(.blue)
                        .onTapGesture {
                            element.tappedOnWebsite(url: element.website)
                        }
                }
            }
        }
    }
}

struct NYCSchoolView_Previews: PreviewProvider {
    static var previews: some View {
        NYCSchoolView()
    }
}
